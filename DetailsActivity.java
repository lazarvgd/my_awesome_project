package com.example.svarog.project01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    private TextView mMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        mMessage = (TextView) findViewById(R.id.show_msg_tv);

        getIntent().getExtras();
        String msg="";
        if(getIntent().hasExtra("test")){
            msg = ""+ getIntent().getIntExtra("test", 42);
        }
        mMessage.setText(msg.toString());
    }
}
