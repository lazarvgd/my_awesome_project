package com.example.svarog.project01;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by svarog on 30.9.17..
 */

public class DownloadIntent extends IntentService {
    private final static String MOST_POPULAR = "popular";
    private final static String TOP_RATED = "top_rated";

    private final static String API_BASE_URL = "http://api.themoviedb.org/";
    private Response<List<JsonRoot>> response;




    public DownloadIntent() {
        super("DownloadService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.i("D_SERVICE", "PREPARE");
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(
                                GsonConverterFactory.create()
                        );

        Retrofit retrofit =builder.client(httpClient.build()).build();
        DownloadMovieList downloadMovieList = retrofit.create(DownloadMovieList.class);

        Call<List<JsonRoot>> request = downloadMovieList.getMovies(TOP_RATED);


        try {
            Log.i("Async", "Requesting...");

            response =  request.execute();
            Log.i("Async", "Response is : " + response.message());

            if(response.isSuccessful()){
//                return response.body();
                Log.i("Async", "DONE : " + response.body());

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
