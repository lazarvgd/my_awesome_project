package com.example.svarog.project01;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by svarog on 27.9.17..
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    private final ListItemClickListener listener;
    private int numberOfelements;


    public MovieAdapter(int numberOfelements, ListItemClickListener listener) {
        this.numberOfelements = numberOfelements;
        this.listener = listener;

    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.layout_movie, parent, false);
        MovieViewHolder holder = new MovieViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {

        holder.bind("test " + position);

    }

    @Override
    public int getItemCount() {
        return numberOfelements;
    }

    public interface ListItemClickListener {
        void onListItemClick(int clickedItemIndex);
    }

    class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView movieName;

        public MovieViewHolder(View itemView) {
            super(itemView);
            movieName = itemView.findViewById(R.id.movie_name);
            itemView.setOnClickListener(this);
        }

        public void bind(String name) {
            movieName.setText(name);
        }

        @Override
        public void onClick(View view) {
            Log.i("Adapter", "click");
            listener.onListItemClick(getAdapterPosition());
        }
    }

}
