package com.example.svarog.project01;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements MovieAdapter.ListItemClickListener {

    private RecyclerView recyclerView;
    private MovieAdapter adapter;

    private final static String MOST_POPULAR = "popular";
    private final static String TOP_RATED = "top_rated";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview_layout);
        adapter = new MovieAdapter(10, this);
        GridLayoutManager manager = new GridLayoutManager(this, 3);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.most_popular:
                Toast.makeText(this, "MOST POPULAR", Toast.LENGTH_SHORT).show();
                break;
            case R.id.top_rated:
                FetchMovieData fetchMovieData = new FetchMovieData();
//                fetchMovieData.execute(TOP_RATED);
                Intent downloadIntent = new Intent(this, DownloadIntent.class);
                Log.i("main", "downloading");
                startService(downloadIntent);
//                Log.i("MAIN" , fetchMovieData.execute(TOP_RATED).toString());
                break;
        }
        return true;
    }

    @Override
    public void onListItemClick(int clickedItemIndex) {
        Log.i("Main","clicked "+clickedItemIndex);
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("test", 12345);
        startActivity(intent);

    }

}
